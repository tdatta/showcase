var done_binding = false;


function get_index(){
  console.log("index function called");
  $(".forms").hide();
  $(".entry_form").hide();
  // call is logged in
  $.ajax({
    dataType: 'json',
    type: 'get',
    url: '/is_logged_in/',
    async: false,
    success: function(data,status) {
      if (data['logged_in']=="yes") {
        // call another ajax here
        $.ajax({
          dataType: 'json',
          type: 'get',
          url: '/jogging_entry/index/',
          async: false,
          success: function(data, status)
          {
            fill_table_from_json(data);
            create_new_logout_button();
            create_new_entry_form();
            $(".entry_form").show();
            bind_all_functions();
          }});
      }
      else{
        create_login_register();
        bind_login();
        bind_register();
         $("#login_click").trigger("click");
        
      }}
    // end success
  });// end ajax called
};




function create_login_register(){
  $(".top_buttons").empty().show();
  $(".container").empty().show();
  $(".forms").empty().show();
  $(".top_buttons").html('<a id="login_click">login</a> <a id="register_click">register</a>');
  bind_login();
  bind_register();
}
function getCookie(name) {
  var cookieValue = null;
  if (document.cookie && document.cookie != '') {
    var cookies = document.cookie.split(';');
    for (var i = 0; i < cookies.length; i++) {
      var cookie = jQuery.trim(cookies[i]);
      // Does this cookie string begin with the name we want?
      if (cookie.substring(0, name.length + 1) == (name + '=')) {
        cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
        break;
      }
    }
  }
  return cookieValue;
};

function create_login_form(){
  // ajax call
  $.ajax({
    //dataType: 'json',
    type: 'get',
    url: '/login/',
    async: false,
    success: function(data, status)
    {
      $(".forms").empty();
      $(".forms").html(data);
      bind_login_submit();

    }});
  // end call
}


function create_register_form(){
  // ajax call
  $.ajax({
    //dataType: 'json',
    type: 'get',
    url: '/register/',
    async: false,
    success: function(data, status)
    {
      $(".forms").empty().show();
      $(".forms").html(data);
      bind_register_submit();

    }});
  // end call
}



function create_new_entry_form(){
  // ajax call
  $.ajax({
    //dataType: 'json',
    type: 'get',
    url: '/jogging_entry/new/',
    async: false,
    success: function(data, status)
    {
      $(".entry_form").empty();
      $(".entry_form").html(data);

    }});
  // end call
};

function fill_table_from_json(jdata){
  console.log(jdata);
  $(".container").empty().show();
  $(".container").html("<table id='jogging_entries' class='tablegr'><table>");
  $('#jogging_entries').append("<tr><th>Date</th><th>Duration (hh:mm:ss)</th><th>Distance (km)</th><th>Average Speed(km/h)</th><th colspan=2>Actions</th> </tr>");
  var table_entry = ''
  $(jdata).each(function(idx, elem){
    table_entry = '<tr data-id ='+elem.id+'><td id=date> '+elem.date+' </td>'
    table_entry += '<td id=duration>'+elem.duration+'</td>';
    table_entry += '<td id=distance>'+elem.distance+ '</td>'
    table_entry += '<td id=Average Speed>'+elem.average_speed+ '</td>'
    table_entry += '<td id=edit> <button class="edit" id='+elem.id+'> edit </button> </td>';
    table_entry += '<td id=delete> <button class="delete" id='+elem.id+'> remove </button> </td></tr>';
    $('#jogging_entries').append(table_entry);
    console.log(table_entry);
    table_entry ='';
  });
};

function create_new_logout_button(){
  $(".top_buttons").empty();
  $(".top_buttons").append("<a id=logout>logout</a>") // <button id=logout class='button-style'>logout</button>");
  bind_logout();
};

function create_submit_entry_button(){
  $(".top_buttons").append("<button id=submit_entry>save entry</button>");
};
/// button clicks

function submit_entry_click(action_url){
  $("#submit_entry").click(function(){
    // ajax call
    var post_data = $(".forms").serializeArray();
    $.ajax({
      //dataType: 'json',
      type: 'post',
      url: action_url,
      async: false,
      data: post_data,
      success: function(data, status)
      {
        $(".forms").empty().hide();
        get_index();
        create_submit_entry_button();

      }});
    // end call
  })

};

function edit_click(){
  $(".edit").click(function(){
    // create a form with that edit
    // call edit
    get_index();
    var pkid = $(this).parent().parent().index();
    console.log(pkid)
    //$(".forms").hide();
    make_row_editable(pkid);

  }
                  )
};

function make_row_editable(row) {
  var x = document.getElementById("jogging_entries").rows[row].cells
  $(x).each(function() {
    if($(this).attr("id")=="edit"){
      var pkid = $(this).children()[0].id;
      var next_html = '<button class="update" id='+pkid+'> update </button> </td>'
      $(this).html(next_html);
      update_click();
    }
    else if($(this).attr("id")=="delete"){
      var pkid = $(this).children()[0].id;
      var next_html = '<button class="cancel" id='+pkid+'> cancel </button> </td>'
      $(this).html(next_html);
      bind_cancel_click();
    }else{
      $(this).html('<input type="text" value="' + $(this).html() + '" />');}
  });
}

function bind_cancel_click(){
  $(".cancel").click(function(){
    $(".forms").show();
    get_index();})
};

function create_form_from_row(nrow){
  // ajax call
  $.ajax({
    //dataType: 'json',
    type: 'get',
    url: '/jogging_entry/new/',
    async: false,
    success: function(data, status)
    {
      $(".hidden_forms").empty();
      $(".hidden_forms").html(data);
      var row = document.getElementById("jogging_entries").rows[nrow].cells;
      var date = new Date(Date.parse(row[0].children[0]["value"]))
      var duration = row[1].children[0]["value"]
      var distance = parseInt(row[2].children[0]["value"])
      $(".hidden_forms").find("#entry_form").find("#id_date").val(date.getFullYear()+"-"+(date.getMonth()+1)+"-"+date.getDate())
      $(".hidden_forms").find("#entry_form").find("#id_duration").val(duration)
      $(".hidden_forms").find("#entry_form").find("#id_distance").val(distance)
      // $("#id_date").html(update_form).attr("value",date);
      // $("#id_duration").html(update_form).attr("value",duration);
      // $("#id_distance").html(update_form).attr("value",distance);

    }});
};

function update_click(){
  $(".update").click(function(){
    var pkid = $(this).attr("id");
    var row_id = $(this).parent().parent().index();
    create_form_from_row(row_id);
    // edit call
    var entry_form = $(".hidden_forms").children()
    if (entry_form){
      entry_form = entry_form.serializeArray();
      $.ajax({
	dataType: 'json',
	type: 'post',
	url: 'jogging_entry/update/'+pkid+'/JSON/',
	data: entry_form,
	async: false,
	beforeSend: function(xhr,settings){
	  var csrftoken = getCookie('csrftoken');
	  xhr.setRequestHeader("X-CSRFToken",csrftoken);
	},
	success: function(data,status,xhr){
          get_index();
	},
	error: function(data, status, xhr){
	  alert(data);
	}
      });
      return false;
    }
    // end call
}
                    )};

function bind_logout(){
  $("#logout").click(function(){
    $(".entry_forms").hide();
    $.ajax({
      dataType: 'json',
      type: 'get',
      url: '/logout/',
      async: false,
      success: function(data,status) {
        if (data['logged_out']=="true") { get_index();}}
    })})};

function bind_login(){
  $("#login_click").click(function(){
    create_login_form();
  })
}

function bind_register(){
 $("#register_click").click(function(){ 
   create_register_form();
  })
}
function delete_click(){
  $(".delete").click(function(){
    var pkid = $(this).attr("id")
    $.ajax({
      //dataType: 'json',
      type: 'get',
      url: 'jogging_entry/delete/'+pkid+'/JSON/',
      async: false,
      success: function(data, status)
      {
        get_index();
      }});
  }
                    )};

function bind_register_submit(){

  var register_form =$("#register_form")
  if (register_form){
    register_form.submit(function(){
      register_form = register_form.serializeArray();
      $.ajax({
        dataType: 'json',
        type: 'post',
        url: "/register/",
        data: register_form,
        async: false,
        beforeSend: function(xhr,settings){
          var csrftoken = getCookie('csrftoken');
          xhr.setRequestHeader("X-CSRFToken",csrftoken);
        },
        success: function(data,status,xhr){
          alert("You have registered sucessfully for our service.You will be now redirected to your personal page");
          get_index();
        },
        error: function(data, status, xhr){
          alert("Registration failed with reason:");
        }
      });
      return false;
    })
  }};


function bind_login_submit(){

  var login_form =$("#login_form")
  if (login_form){
    login_form.submit(function(){
      login_form = login_form.serializeArray();
      $.ajax({
        dataType: 'json',
        type: 'post',
        url: "/login/",
        data: login_form,
        async: false,
        beforeSend: function(xhr,settings){
          var csrftoken = getCookie('csrftoken');
          xhr.setRequestHeader("X-CSRFToken",csrftoken);
        },
        success: function(data,status,xhr){
          get_index();
        },
        error: function(data, status, xhr){
          alert(data);
        }
      });
      return false;
    })
  }};

function create_entry_click(){
  var entry_form = $("#entry_form")
  if (entry_form){
    entry_form.submit(function(){
      entry_form = entry_form.serializeArray();
      $.ajax({
	dataType: 'json',
	type: 'post',
	url: "/jogging_entry/create/",
	data: entry_form,
	async: false,
	beforeSend: function(xhr,settings){
	  var csrftoken = getCookie('csrftoken');
	  xhr.setRequestHeader("X-CSRFToken",csrftoken);
	},
	success: function(data,status,xhr){
          get_index();
	},
	error: function(data, status, xhr){
	  alert(data);
	}
      });
      return false;
    })
  }};

function bind_all_functions()
{
  edit_click();
  delete_click();
  bind_logout();
  //new_entry_create();
  create_entry_click();
}
// ready
$(document).ready(function(){

  get_index();
});
