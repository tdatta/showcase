__author__ = 'Tanmay'

from django.shortcuts import render
from django.http import HttpResponseRedirect, HttpResponse
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.template.response import TemplateResponse
from django.core import serializers
from django.contrib.auth.decorators import login_required
from django.contrib.auth import login, authenticate, logout
from .models import JoggingEntry
from .jogging_forms import JoggingEntryForm
import json


def home(request):
    return TemplateResponse(request, 'index.html')


def is_logged(request):
    return HttpResponse(json.dumps({"logged_in": "no"})) \
        if request.user.is_anonymous() \
        else HttpResponse(json.dumps({"logged_in": "yes"}))


def string_from_seconds(sec):
    hours, remainder = divmod(sec, 3600)
    minutes, seconds = divmod(remainder, 60)
    return '%s:%s:%s' % (hours, minutes, seconds)


@login_required()
def index(request, return_type='JSON'):
    user = request.user
    entries = JoggingEntry.objects.filter(user=user)
    if return_type.upper() == 'JSON':
        print "going to give data"
        d = []
        for e in entries:
            duration = string_from_seconds(e.duration.seconds)
            d.append({'date': e.date.strftime("%b %d, %Y"),\
            'distance': e.distance, 'duration': duration,\
            'average_speed': round(e.average_speed,2), 'id': e.id})
        data = d
        # dynatable specific
        # data = {'records':d}
        # data["queryRecordCount"] = count
        # data["totalRecordCount"] = count
        # end dynatable
        # data = serializers.serialize('json',data)
        # print data
        return HttpResponse(json.dumps(data))
        # return HttpResponse(data, content_type="application/json")
    else:
        return render(request, "/index.html", {'entries': entries})


def my_logout(request):
    logout(request)
    ret_val = json.dumps({"logged_out": "true"})
    return HttpResponse(ret_val)


def my_login(request):
    if request.method == 'POST':
        print request.POST
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(username=username, password=password)
        if user is not None:
            if user.is_active:
                login(request, user)
                ret_val = json.dumps({"logged_in": "true"})
                return HttpResponse(ret_val)
            # Redirect to a success page.
            else:
                return HttpResponse(json.dumps({"logged_in": "not_active"}))
                # Return a 'disabled account' error message
        else:

            return HttpResponse(json.dumps({"logged_in": "false"}))
            # Return an 'invalid login' error message.
            # return TemplateResponse(request, 'registration/login.html')
    elif request.method=='GET':
        form = AuthenticationForm()
        return render(request, 'registration/login.html', {'form': form})


def my_register(request):
    """
    :param request: From the form
    :return: next page renderd
    """
    if request.method == 'POST':
        new_user_form = UserCreationForm(request.POST)
        if new_user_form.is_valid():
            new_user = new_user_form.save()
            ret_val = json.dumps({"registered": "true"})
            new_user.backend = 'django.contrib.auth.backends.ModelBackend'
            login(request, new_user)
            return HttpResponse(ret_val)
        else:
            ret_val = json.dumps({"registered": "false"})
            return HttpResponse(ret_val)
    elif request.method == 'GET':
        form = UserCreationForm()
        return render(request, 'registration/registration.html',{'form': form})


@login_required(login_url='/login/')
def entry_new(request):
    form = JoggingEntryForm()
    return render(request, 'jogging_entry/createEntry.html', {'form': form})



@login_required(login_url='/login/')
def entry_create(request):

    form = JoggingEntryForm(request.POST)
    if request.method == 'POST':

        # check whether it's valid:
        if form.is_valid():
            # process the data in form.cleaned_data as required
            # ...
            # redirect to a new URL:
            entry_obj = form.save(commit=False)
            entry_obj.user = request.user
            entry_obj.save()
            return HttpResponseRedirect('/jogging_entry/index/')
        else:
            form = JoggingEntryForm()
            return render(request, 'jogging_entry/createEntry.html', {'form': form})




@login_required(login_url='/login/')
def entry_update(request, entry_id, return_type='JSON'):
    if request.method == "POST":
        entry_instance = JoggingEntry.objects.get(id=entry_id)
        print "inside update"
        update_form = JoggingEntryForm(instance=entry_instance, data=request.POST)
        if update_form.is_valid():
            print "done"
            print entry_id
            update_form.save()
        entries = JoggingEntry.objects.filter(user=request.user)
        if return_type.upper() == 'JSON':
            data = serializers.serialize('json', entries)
            return HttpResponse(data, content_type="application/json")
        else:
            return render(request, "/index.html", {'entries': entries})



@login_required(login_url='/login/')
def entry_delete(request, entry_id, return_type='JSON',):

    entries = JoggingEntry.objects.filter(user=request.user)
    if request.method == 'GET':
        JoggingEntry.objects.filter(id=entry_id).delete()
    if return_type.upper() == 'JSON':
        data = serializers.serialize('json', entries)
        return HttpResponse(data, content_type="application/json")
    else:
        return render(request, "/index.html", {'entries': entries})
