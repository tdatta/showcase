__author__ = 'Tanmay'
from models import JoggingEntry
from django.contrib.auth.models import User
from django.test import Client
import unittest


def setup():
    test_user = User.objects.get(username='test')
    je1 = JoggingEntry(user=test_user, distance=4)
    je1.save()
    je2 = JoggingEntry(user=test_user, distance=10)
    je2.save()

class Login_Tests(unittest.TestCase):

    def setup(self):
        self.resp_dict={'logged_in':'yes'}
        
    def login_test():
        client = Client()
        client.login(username='test', password='test')
        resp = client.get('/is_logged_in')
        resp = eval(resp)

