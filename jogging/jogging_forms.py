__author__ = 'Tanmay'
from .models import JoggingEntry
from django.forms import ModelForm


class JoggingEntryForm(ModelForm):
    class Meta:
        model = JoggingEntry
        fields = ['date', 'duration', 'distance']

    
