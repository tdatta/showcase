__author__ = 'Tanmay'

from django.contrib.auth.models import User
from django.db import models
import timedelta
from datetime import datetime
# Create your models here.


class JoggingEntry(models.Model):
    """
    Entry model
    """
    date = models.DateField(verbose_name='Date', default=datetime.now())
    distance = models.FloatField(verbose_name='Distance done', default=0)
    duration = timedelta.fields.TimedeltaField(verbose_name='Duration for jogging', default=timedelta.parse("0 h"))
    user = models.ForeignKey(User, related_name='entries')

    @property
    def average_speed(self):
        """
        Returns speed in kmph
        """
        if self.duration.total_seconds() >0:
            return self.distance/(self.duration.total_seconds()/3600)
        else:
            return 0