from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.contrib.auth.views import login, logout
import jogging_entry_views
from .jogging_entry_views import *

urlpatterns = patterns('',
    # Examples:
    # url(r'^.*o/g/', include('blog.urls')),

    url(r'^$', jogging_entry_views.home, name='home'),
    url(r'^is_logged_in/$', jogging_entry_views.is_logged),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^login/$', my_login, name="login_user"),
    url(r'^logout/$', my_logout, name="logout_user"),
    url(r'^register/$', my_register, name="register_user"),
    url(r'jogging_entry/index/$', index),
    url(r'jogging_entry/index/(.*)', index),
    url(r'jogging_entry/new/$', entry_new),
    url(r'jogging_entry/create/$', entry_create),
    url(r'jogging_entry/update/(.*)/(.*)/$', entry_update),
    url(r'jogging_entry/delete/(.*)/(.*)/$', entry_delete),
)
